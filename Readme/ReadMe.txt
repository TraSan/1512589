1512589
Sằn Thanh Trà
mrteatran@gmail.com
Các chức năng đã làm được:
- Tạo tree view hiển thị các tag
- Nhập và lưu note theo các tag
- Hiển thị các tag đã lưu
- Nhấn chuột lên tag bên tree view hiển thị tất cả các thông tin sơ lược về các note chứa trong tag
- Nhấn chuột lên các note bên preview sẽ hiển thị nội dung của note
- Vẽ biểu đồ tự động tùy theo số lượng tag kèm chú thích

Luồng xử lí chính:
a,Tạo note :
- Người dùng nhập nội dung note vào note
- Nhập tag vào  ô tag
- Nhấn save
b, Xem các note trong trong tag
- nhấn chuột trái lên tag bên phần tree view
- nội dung được hiển thị trong phần preview

c, xem nội dung của note:
- Nhấn chuột vào note bên phần preview
- Nội dung note được hiển thị
- Nhấn OK để thoát note

d, người dùng không nhập note mà tiến hành lưu chương trình sẽ thông báo lỗi

Link youtube: https://youtu.be/maIjka8tMWE
Link B: https://TraSan@bitbucket.org/TraSan/1512589.git


